package com.tcb.CountAverageSpeedForEachHour;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CountAverageSpeedFromParquetFileReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

	@Override
	public void reduce(Text pickup_hour, Iterable<DoubleWritable> values,
			Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context)
			throws IOException, InterruptedException {

		for (DoubleWritable value : values) {

			double speed = Double.parseDouble(value.toString());
			context.write(pickup_hour, new DoubleWritable(speed));
			//System.out.println("done !");
		}
	}

}
