package com.tcb.CountAverageSpeedForEachHour;

import org.apache.commons.net.ntp.TimeStamp;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.parquet.VersionParser.VersionParseException;
import org.apache.parquet.hadoop.example.ExampleInputFormat;

public class AverageSpeedDriver {

	public final static String INPUT_PATH = "hdfs://192.168.1.113:8020/user/nacer/test/";
	public final static String OUTPUT_PATH = "file:///home/nassereddine/workspace/CountAverageSpeedForEachHour/target/";

	// public final static String INPUT_PATH
	// ="/home/nassereddine/Téléchargements/part-r-00483-ec9cbb65-519d-4bdb-a918-72e2364c144c.snappy.parquet";
	public static void main(String[] args) throws Exception, VersionParseException {

		Configuration c = new Configuration();
		Job j = Job.getInstance(c);

		FileInputFormat.setInputPaths(j, new Path(INPUT_PATH));
		FileOutputFormat.setOutputPath(j, new Path(OUTPUT_PATH + TimeStamp.getCurrentTime()));
		j.setJarByClass(AverageSpeedDriver.class);
		j.setMapperClass(CountAverageSpeedFromParquetFileMapper.class);
		j.setCombinerClass(CountAverageSpeedCombiner.class);

		j.setReducerClass(CountAverageSpeedFromParquetFileReducer.class);
		j.setInputFormatClass(ExampleInputFormat.class);
		j.setMapOutputKeyClass(Text.class);
		j.setMapOutputValueClass(DoubleWritable.class);
		j.setOutputKeyClass(Text.class);
		j.setOutputValueClass(DoubleWritable.class);
		System.exit(j.waitForCompletion(true) ? 0 : 1);

	}

}
