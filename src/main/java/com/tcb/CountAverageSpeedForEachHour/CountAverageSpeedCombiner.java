package com.tcb.CountAverageSpeedForEachHour;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CountAverageSpeedCombiner extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

	@Override
	public void reduce(Text key, Iterable<DoubleWritable> values,
			Reducer<Text, DoubleWritable, Text, DoubleWritable>.Context context)
			throws IOException, InterruptedException {
		for (DoubleWritable value : values) {

			double speed = Double.parseDouble(value.toString())*60;
			context.write(new Text(key), new DoubleWritable(speed));
			//System.out.println(" The program is calling  me !!!!! ");
		}

	}
}
