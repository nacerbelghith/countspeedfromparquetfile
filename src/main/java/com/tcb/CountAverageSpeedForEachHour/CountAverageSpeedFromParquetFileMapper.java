package com.tcb.CountAverageSpeedForEachHour;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.parquet.example.data.Group;

public class CountAverageSpeedFromParquetFileMapper extends Mapper<Text, Group, Text, DoubleWritable> {
	@Override
	public void map(Text key, Group value, Mapper<Text, Group, Text, DoubleWritable>.Context context)
			throws IOException, InterruptedException {
		TimeConverter timeConverter = new TimeConverter();
		Double d = 234.12413;
		String[] splitter = d.toString().split("\\.");
		splitter[0].length();   // Before Decimal Count
		splitter[1].length();   // After  Decimal Count
		
		int pickup_hour = 0;
		int pickup_min = 0;
		int pickup_sec = 0;
		double pickup_time = 0;
		int dropoff_min = 0;
		int dropoff_sec = 0;
		double dropoff_time = 0;
		double trip_time = 0;
		double trip_distance = 0;
		double trip_speed = 0;
		try {
			String partOfTheKey = TimeConverter
					.getDate(timeConverter.getDateTimeValueFromBinary(value.getInt96("pickup_datetime", 0), true))
					.toString();

			pickup_hour = TimeConverter
					.getHour(timeConverter.getDateTimeValueFromBinary(value.getInt96("pickup_datetime", 0), true));

			pickup_min = TimeConverter
					.getMinute(timeConverter.getDateTimeValueFromBinary(value.getInt96("pickup_datetime", 0), true));

			pickup_sec = TimeConverter
					.getSeconde(timeConverter.getDateTimeValueFromBinary(value.getInt96("pickup_datetime", 0), true));

			pickup_time = (pickup_min  + (pickup_sec / 60));
			
			//System.out.println("Pickup time is " + pickup_time);

			dropoff_min = TimeConverter
					.getMinute(timeConverter.getDateTimeValueFromBinary(value.getInt96("dropoff_datetime", 0), true));

			dropoff_sec = TimeConverter
					.getSeconde(timeConverter.getDateTimeValueFromBinary(value.getInt96("dropoff_datetime", 0), true));

			dropoff_time = (dropoff_min / 60) + (dropoff_sec / 3600);
			//System.out.println("pickup secondes :" + pickup_sec);

			trip_time = pickup_time;
			
			trip_distance = value.getFloat("trip_distance", 0);
			//System.out.println("trip distance is : " + trip_distance);
			if ((trip_time != 0)) {
			//	System.out.println(trip_time);

				trip_speed = trip_distance / trip_time;
			//	System.out.println(partOfTheKey + " I have done it ");

			}
			if(trip_speed*60 <50 && trip_speed > 0){
             System.out.println("it is the case");
			String str = partOfTheKey + "  " + pickup_hour + "   Speed";
			Text outputKey = new Text(str);
			DoubleWritable outputValue = new DoubleWritable(trip_speed);
			// System.out.println(value.toString());
			context.write(outputKey, outputValue);
			}

		} catch (Exception e) {

		}
	}

}
