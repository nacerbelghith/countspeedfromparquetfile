package com.tcb.CountAverageSpeedForEachHour;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;

import org.apache.commons.lang.time.DateUtils;
import org.apache.parquet.example.data.simple.NanoTime;
import org.apache.parquet.io.api.Binary;

//Timestamp converter: convert a biary format to dateS
public class TimeConverter {

    static ZoneId zone = ZoneId.of("Africa/Tunis");
    public static final long NANOS_PER_MILLISECOND = 1000000;
    public static final long JULIAN_DAY_NUMBER_FOR_UNIX_EPOCH = 2440588;

    public long getDateTimeValueFromBinary(Binary binaryTimeStampValue, boolean retainLocalTimezone) {
        // This method represents binaryTimeStampValue
        NanoTime nt = NanoTime.fromBinary(binaryTimeStampValue);
        int julianDay = nt.getJulianDay();
        long nanosOfDay = nt.getTimeOfDayNanos();
        long dateTime = (julianDay - JULIAN_DAY_NUMBER_FOR_UNIX_EPOCH) * DateUtils.MILLIS_PER_DAY
                + nanosOfDay / NANOS_PER_MILLISECOND;
        if (retainLocalTimezone) {

            Instant instant = Instant.ofEpochMilli(dateTime).atZone(ZoneOffset.UTC).withZoneSameLocal(zone)
                    .toInstant();
            return instant.toEpochMilli();
        } else {
            return dateTime;
        }
        
    }
    
    public static Timestamp getTimestamp(long dateTime){
    	Timestamp stamp = new Timestamp(dateTime);
    	return stamp;
    }
    
    public static int getHour(long dateTime)
    {
    	Timestamp stamp = new Timestamp(dateTime);
    	int hour = Integer.parseInt( (stamp.toString().substring(11, 13)));
    	return hour;
    }
    
    public static int getMinute(long dateTime)
    {
    	Timestamp stamp = new Timestamp(dateTime);
    	int minute = Integer.parseInt( (stamp.toString().substring(14, 16)));
    	return minute;
    }
    
    public static int getSeconde(long dateTime)
    {
    	Timestamp stamp = new Timestamp(dateTime);
    	int seconde = Integer.parseInt( (stamp.toString().substring(17, 19)));
    	return seconde;
    }

    
    public static Date getDate(long dateTime)
    {
    	Timestamp stamp = new Timestamp(dateTime);
    	Date date = new Date(stamp.getTime());
    	return date;   	
    }
    
    //extract the year number from dateTime (TimeStamp format)
    public static int getYear(long dateTime){
    	Timestamp stamp = new Timestamp(dateTime);
    	Date date = new Date(stamp.getTime());
    	String years = date.toString().substring(0, 4);
    	int year = Integer.parseInt(years);
    	return year;
    }
    
    //extract the month number from dateTime (TimeStamp format)
    public static int getMonth(long dateTime){
    	Timestamp stamp = new Timestamp(dateTime);
    	Date date = new Date(stamp.getTime());
    	String months = date.toString().substring(5, 7);
    	int month = Integer.parseInt(months);
    	return month;
    }
    
    //extract the year and the month from dateTime (TimeStamp format)
    public static String getMonthYear(long dateTime){
    	Timestamp stamp = new Timestamp(dateTime);
    	Date date = new Date(stamp.getTime());
    	String monthyear = date.toString().substring(0, 7);
    	return monthyear;

    }
}
